<?php

namespace Drupal\pager_for_node;

use Symfony\Contracts\EventDispatcher\Event;
use Drupal\node\NodeInterface;

/**
 * Defines a Pager for node Node event.
 */
class PagerForNodeEvent extends Event {

  protected $queries;
  protected $node;

  /**
   * PagerForNodeEvent constructor.
   *
   * @param array $queries
   *   The queries for this event.
   * @param \Drupal\node\NodeInterface $node
   *   The node object.
   */
  public function __construct(array $queries, NodeInterface $node) {
    $this->queries = $queries;
    $this->node = $node;
  }

  /**
   * Getter for query array.
   *
   * @return array
   *   The queries for this event.
   */
  public function getQueries() {
    return $this->queries;
  }

  /**
   * Setter for query array.
   *
   * @param array $queries
   *   The queries for this event.
   */
  public function setQueries(array $queries) {
    $this->queries = $queries;
  }

  /**
   * Getter for node.
   *
   * @return \Drupal\node\NodeInterface
   *   The node object for this event.
   */
  public function getNode() {
    return $this->node;
  }
}