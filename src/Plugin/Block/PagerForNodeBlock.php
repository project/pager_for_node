<?php

namespace Drupal\pager_for_node\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\pager_for_node\PagerForNodePager;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a "PagerForNode" block.
 *
 * @Block(
 *   id = "pagerfornode_block",
 *   admin_label = @Translation("Pagerfornode Block")
 * )
 */
class PagerForNodeBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The http request.
   *
   * @var null|\Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The pager_for_node pager service.
   *
   * @var \Drupal\pager_for_node\PagerForNodePager
   */
  protected $pagerForNodePager;

  /**
   * The pagerfornode Settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $PagerForNodeSettings;

  /**
   * The current route service.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RequestStack $requestStack, PagerForNodePager $PagerForNodePager, ConfigFactoryInterface $configFactoryInterface, CurrentRouteMatch $routeMatch) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->request = $requestStack->getCurrentRequest();
    $this->PagerForNodePager = $PagerForNodePager;
    $this->PagerForNodeSettings = $configFactoryInterface->get('pager_for_node.settings');
    $this->routeMatch = $routeMatch;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack'),
      $container->get('pager_for_node.pager'),
      $container->get('config.factory'),
      $container->get('current_route_match')
    );
  }

  /**
   * Implements \Drupal\block\BlockBase::build().
   */
  public function build() {
    $build = [];
    if ($node = $this->request->attributes->get('node')) {
      if ($this->PagerForNodePager->pager_for_node_use_pager($node)) {
        $build = [
          '#theme'    => 'pager_for_node',
          '#list'     => $this->PagerForNodePager->pager_for_node_build_list($node),
          '#node'     => $node,
          '#attached' => [
            'library' => [
              'pager_for_node/drupal.pager_for_node',
            ],
          ],
        ];
        if (is_object($node)) {
          if ($this->PagerForNodeSettings->get('pager_for_node_head_' . $node->getType())) {
            $links = $this->PagerForNodePager->pager_for_node_build_list($node);
            if (!empty($links['prev']['nid'])) {
              $build['#attached']['html_head_link'][][] = [
                'rel' => 'prev',
                'href' => Url::fromRoute('entity.node.canonical', ['node' => $links['prev']['nid']])->toString(),
              ];
            }
            if (!empty($links['next']['nid'])) {
              $build['#attached']['html_head_link'][][] = [
                'rel' => 'next',
                'href' => Url::fromRoute('entity.node.canonical', ['node' => $links['next']['nid']])->toString(),
              ];
            }
          }
        }
      }
    }

    return $build;
  }

  /**
   * Implements \Drupal\Core\Entity\Entity::getCacheTags().
   */
  public function getCacheTags() {
    if ($node = $this->routeMatch->getParameter('node')) {
      $cache_tags = Cache::mergeTags($node->getCacheTags(), ['node_list']);
      return Cache::mergeTags(parent::getCacheTags(), $cache_tags);
    }
    else {
      return parent::getCacheTags();
    }
  }

  /**
   * Implements \Drupal\Core\Entity\Entity::getCacheContexts().
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['route']);
  }
}