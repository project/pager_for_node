# Pager For Node

## Table of contents

- Introduction
- Installation
- Requirements
- Configuration
- Maintainers


## Introduction

Enables administrators to set up personalized pagers for browsing node listings.
This feature empowers administrators to create customized pager systems for navigating lists of nodes.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

• Download and extract files in the module folder.
• Enable the module from the module list.
• Navigate to admin > structure > content type, and click on edit in which you want to add pager.
• Click on Pager for node settings tab, check Build a pager for this content type checkbox.
• Check Add semantic previous and next links to the document HEAD checkbox.
• Then after, add label for previous link and next link.
• Click on save.


## Maintainers:

- [Rajesh Bhimani](https://www.drupal.org/u/rajesh-bhimani)

Supporting organizations:
* [Skynet Technologies USA LLC](https://www.skynettechnologies.com)
