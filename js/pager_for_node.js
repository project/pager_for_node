/**
 * @file
 * Adds left- and right-arrow button support to pager_for_node.
 */

(function ($, Drupal) {
  'use strict';
  Drupal.behaviors.pager_for_node = {
    attach: function (context, settings) {
      $(document).keydown(function (event) {
        switch (event.which) {
          case 37:
            leftArrowPressed();
            break;

          case 39:
            rightArrowPressed();
            break;
        }
      });

      var hammer = new Hammer(document);
      hammer.on('swipeleft', function () {
        leftArrowPressed();
      });

      hammer.on('swiperight', function () {
        rightArrowPressed();
      });

      function leftArrowPressed() {
        if ($('.pager_for_node-previous a', context).length) {
          window.location = $('.pager_for_node-previous a', context).attr('href');
        }
      }

      function rightArrowPressed() {
        if ($('.pager_for_node-next a', context).length) {
          window.location = $('.pager_for_node-next a', context).attr('href');
        }
      }
    }
  };
})(jQuery, Drupal);
