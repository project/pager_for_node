<?php

/**
 * @file
 * Allows administrators to add previous/next pagers to any node type.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Url;

/**
 * Implements hook_theme().
 */
function pager_for_node_theme() {
  return [
    'pager_for_node' => [
      'variables' => [
        'list' => [],
        'node' => NULL,
      ],
      'template' => 'pager_for_node',
    ],
  ];
}

/**
 * Implements hook_theme_suggestions().
 */
function pager_for_node_theme_suggestions_pager_for_node(array $variables) {
  $suggestions = [];
  $node = $variables['node'];

  $suggestions[] = 'pager_for_node__' . $node->bundle();
  $suggestions[] = 'pager_for_node__' . $node->bundle() . '__' . $node->id();

  return $suggestions;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function pager_for_node_form_node_type_edit_form_alter(&$form, FormStateInterface $form_state) {
  if (isset($form['type'])) {

    // pager_for_node settings.
    $pager_for_node_settings = \Drupal::config('pager_for_node.settings');
    $pager_for_node_node_type = $pager_for_node_settings->get('pager_for_node_' . $form['type']['#default_value']);
    $pager_for_node_head = $pager_for_node_settings->get('pager_for_node_head_' . $form['type']['#default_value']);
    $pager_for_node_show_empty = $pager_for_node_settings->get('pager_for_node_show_empty_' . $form['type']['#default_value']);
    $pager_for_node_prev_label = $pager_for_node_settings->get('pager_for_node_prev_label_' . $form['type']['#default_value']);
    $pager_for_node_next_label = $pager_for_node_settings->get('pager_for_node_next_label_' . $form['type']['#default_value']);
    $pager_for_node_first_last = $pager_for_node_settings->get('pager_for_node_first_last_' . $form['type']['#default_value']);
    $pager_for_node_first_label = $pager_for_node_settings->get('pager_for_node_first_label_' . $form['type']['#default_value']);
    $pager_for_node_last_label = $pager_for_node_settings->get('pager_for_node_last_label_' . $form['type']['#default_value']);
    $pager_for_node_loop = $pager_for_node_settings->get('pager_for_node_loop_' . $form['type']['#default_value']);
    $pager_for_node_random = $pager_for_node_settings->get('pager_for_node_random_' . $form['type']['#default_value']);
    $pager_for_node_random_label = $pager_for_node_settings->get('pager_for_node_random_label_' . $form['type']['#default_value']);
    $pager_for_node_truncate = $pager_for_node_settings->get('pager_for_node_truncate_' . $form['type']['#default_value']);
    $pager_for_node_ellipse = $pager_for_node_settings->get('pager_for_node_ellipse_' . $form['type']['#default_value']);
    $pager_for_node_swipe = $pager_for_node_settings->get('pager_for_node_press_swipe_' . $form['type']['#default_value']);
    $pager_for_node_custom_sorting = $pager_for_node_settings->get('pager_for_node_custom_sorting_' . $form['type']['#default_value']);
    $pager_for_node_sort = $pager_for_node_settings->get('pager_for_node_sort_' . $form['type']['#default_value']);
    $pager_for_node_order = $pager_for_node_settings->get('pager_for_node_order_' . $form['type']['#default_value']);

    $form['pager_for_node'] = [
      '#type' => 'details',
      '#title' => t('Pager for node settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'additional_settings',
    ];

    $form['pager_for_node']['pager_for_node'] = [
      '#type' => 'checkbox',
      '#title' => t('Build a pager for this content type'),
      '#default_value' => isset($form['pager_for_node']['pager_for_node']) ? $form['pager_for_node']['pager_for_node'] : $pager_for_node_node_type,
    ];

    $form['pager_for_node']['pager_for_node_head'] = [
      '#type' => 'checkbox',
      '#title' => t('Add semantic previous and next links to the document HEAD'),
      '#default_value' => isset($form['pager_for_node']['pager_for_node_head']) ? $form['pager_for_node']['pager_for_node_head'] : $pager_for_node_head,
      '#states' => [
        'visible' => [
          ':input[name=pager_for_node]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['pager_for_node']['pager_for_node_show_empty'] = [
      '#type' => 'checkbox',
      '#title' => t('Show empty links'),
      '#default_value' => isset($form['pager_for_node']['pager_for_node_show_empty']) ? $form['pager_for_node']['pager_for_node_show_empty'] : $pager_for_node_show_empty,
      '#states' => [
        'visible' => [
          ':input[name=pager_for_node]' => ['checked' => TRUE],
        ],
      ],
      '#description' => t('If checked, empty links will be rendered even if there isn\'t a node in the sequence. For example, if there is no "next" node, the "next" label will be shown but without a link. If tokens are being used, it is recommended that this be unchecked.'),
    ];

    $form['pager_for_node']['pager_for_node_prev_label'] = [
      '#type' => 'textfield',
      '#title' => t('Label for "Previous" link'),
      '#description' => t('This field supports tokens.'),
      '#size' => 32,
      '#default_value' => isset($form['pager_for_node']['pager_for_node_prev_label']) ? $form['pager_for_node']['pager_for_node_prev_label'] : $pager_for_node_prev_label,
      '#states' => [
        'visible' => [
          ':input[name=pager_for_node]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['pager_for_node']['pager_for_node_next_label'] = [
      '#type' => 'textfield',
      '#title' => t('Label for "Next" link'),
      '#description' => t('This field supports tokens.'),
      '#size' => 32,
      '#default_value' => isset($form['pager_for_node']['pager_for_node_next_label']) ? $form['pager_for_node']['pager_for_node_next_label'] : $pager_for_node_next_label,
      '#states' => [
        'visible' => [
          ':input[name=pager_for_node]' => ['checked' => TRUE],
        ],
      ],
    ];

    if (\Drupal::moduleHandler()->moduleExists('token')) {
      $form['pager_for_node']['pager_for_node_token'] = [
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#title' => t('Browse available tokens'),
        '#states' => [
          'visible' => [
            ':input[name=pager_for_node]' => ['checked' => TRUE],
          ],
        ],
      ];

      $form['pager_for_node']['pager_for_node_token']['pager_for_node_token_browser'] = [
        '#theme' => 'token_tree_link',
        '#token_types' => ['node'],
      ];
    }

    $form['pager_for_node']['pager_for_node_firstlast'] = [
      '#type' => 'checkbox',
      '#title' => t('Show first/last links'),
      '#default_value' => isset($form['pager_for_node']['pager_for_node_firstlast']) ? $form['pager_for_node']['pager_for_node_firstlast'] : $pager_for_node_first_last,
      '#states' => [
        'visible' => [
          ':input[name=pager_for_node]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['pager_for_node']['pager_for_node_first_label'] = [
      '#type' => 'textfield',
      '#title' => t('Label for "First" link'),
      '#description' => t('This field supports tokens.'),
      '#size' => 32,
      '#default_value' => isset($form['pager_for_node']['pager_for_node_first_label']) ? $form['pager_for_node']['pager_for_node_first_label'] : $pager_for_node_first_label,
      '#states' => [
        'visible' => [
          ':input[name=pager_for_node_firstlast]' => ['checked' => TRUE],
          ':input[name=pager_for_node]' => ['checked' => TRUE],

        ],
      ],
    ];

    $form['pager_for_node']['pager_for_node_last_label'] = [
      '#type' => 'textfield',
      '#title' => t('Label for "Last" link'),
      '#description' => t('This field supports tokens.'),
      '#size' => 32,
      '#default_value' => isset($form['pager_for_node']['pager_for_node_last_label']) ? $form['pager_for_node']['pager_for_node_last_label'] : $pager_for_node_last_label,
      '#states' => [
        'visible' => [
          ':input[name=pager_for_node_firstlast]' => ['checked' => TRUE],
          ':input[name=pager_for_node]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['pager_for_node']['pager_for_node_loop'] = [
      '#type' => 'checkbox',
      '#title' => t('Loop through nodes'),
      '#default_value' => isset($form['pager_for_node']['pager_for_node_loop']) ? $form['pager_for_node']['pager_for_node_loop'] : $pager_for_node_loop,
      '#states' => [
        'visible' => [
          ':input[name=pager_for_node]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['pager_for_node']['pager_for_node_random'] = [
      '#type' => 'checkbox',
      '#title' => t('Show random link'),
      '#default_value' => isset($form['pager_for_node']['pager_for_node_random']) ? $form['pager_for_node']['pager_for_node_random'] : $pager_for_node_random,
      '#states' => [
        'visible' => [
          ':input[name=pager_for_node]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['pager_for_node']['pager_for_node_random_label'] = [
      '#type' => 'textfield',
      '#title' => t('Label for "Random" link'),
      '#size' => 32,
      '#default_value' => isset($form['pager_for_node']['pager_for_node_random_label']) ? $form['pager_for_node']['pager_for_node_random_label'] : $pager_for_node_random_label,
      '#states' => [
        'visible' => [
          ':input[name=pager_for_node_random]' => ['checked' => TRUE],
          ':input[name=pager_for_node]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['pager_for_node']['pager_for_node_truncate'] = [
      '#type' => 'textfield',
      '#title' => t('Truncate label length'),
      '#size' => 32,
      '#default_value' => isset($form['pager_for_node']['pager_for_node_truncate']) ? $form['pager_for_node']['pager_for_node_truncate'] : $pager_for_node_truncate,
      '#description' => t('You may choose to specify a maximum character limit for link labels. Labels exceeding this length will be automatically truncated to fit within the specified limit.'),
      '#states' => [
        'visible' => [
          ':input[name=pager_for_node]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['pager_for_node']['pager_for_node_ellipse'] = [
      '#type' => 'textfield',
      '#title' => t('Truncate ellipse'),
      '#size' => 32,
      '#default_value' => isset($form['pager_for_node']['pager_for_node_ellipse']) ? $form['pager_for_node']['pager_for_node_ellipse'] : $pager_for_node_ellipse,
      '#description' => t('If a maximum label length is set above, an ellipse can be provided here which will be appended to the label after it is shortened.'),
      '#states' => [
        'visible' => [
          ':input[name=pager_for_node]' => ['checked' => TRUE],
        ],
      ],
    ];

    if (\Drupal::moduleHandler()->moduleExists('hammerjs')) {
      $form['pager_for_node']['pager_for_node_press_swipe'] = [
        '#type' => 'checkbox',
        '#title' => t('Support key press and swipe in mobile'),
        '#default_value' => isset($form['pager_for_node']['pager_for_node_press_swipe']) ? $form['pager_for_node']['pager_for_node_press_swipe'] : $pager_for_node_swipe,
      ];
    }

    $form['pager_for_node']['pager_for_node_custom_sorting'] = [
      '#type' => 'checkbox',
      '#title' => t('Sort the pager by something other than ascending post date'),
      '#default_value' => isset($form['pager_for_node']['pager_for_node_custom_sorting']) ? $form['pager_for_node']['pager_for_node_custom_sorting'] : $pager_for_node_custom_sorting,
      '#states' => [
        'visible' => [
          ':input[name=pager_for_node]' => ['checked' => TRUE],
        ],
      ],
    ];

    $sort_options = [];
    $content_type_fields = \Drupal::service('entity_field.manager')
      ->getFieldDefinitions('node', $form['type']['#default_value']);
    foreach ($content_type_fields as $sort_field) {
      if (get_class($sort_field) == 'Drupal\field\Entity\FieldConfig') {
        $itemDefinition = $sort_field->getFieldStorageDefinition();
        $schema_info = $itemDefinition->getSchema();
      }
      else {
        if (get_class($sort_field) == 'Drupal\Core\Field\BaseFieldDefinition') {
          $schema_info = $sort_field->getSchema();
        }
      }
      if (isset($schema_info['columns']['value']) && ($schema_info['columns']['value']['type'] == 'varchar' || $schema_info['columns']['value']['type'] == 'int' || $sort_field->getType() == 'datetime')) {
        $sort_options[$sort_field->getName()] = $sort_field->getLabel();
      }
    }

    $form['pager_for_node']['pager_for_node_sort'] = [
      '#type' => 'select',
      '#title' => t('Pager sort'),
      '#options' => $sort_options,
      '#default_value' => isset($form['pager_for_node']['pager_for_node_sort']) ? $form['pager_for_node']['pager_for_node_sort'] : $pager_for_node_sort,
      '#description' => t('Select a field for sorting the pager'),
      '#states' => [
        'visible' => [
          ':input[name=pager_for_node_custom_sorting]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['pager_for_node']['pager_for_node_order'] = [
      '#type' => 'select',
      '#title' => t('Pager order'),
      '#options' => ['ASC' => t('Ascending'), 'DESC' => t('Descending')],
      '#default_value' => isset($form['pager_for_node']['pager_for_node_order']) ? $form['pager_for_node']['pager_for_node_order'] : $pager_for_node_order,
      '#description' => t('Select a direction to order the pager'),
      '#states' => [
        'visible' => [
          ':input[name=pager_for_node_custom_sorting]' => ['checked' => TRUE],
        ],
      ],
    ];
    //Incorporate a personalized submit handler to store the array of types into the configuration file.
    $form['actions']['submit']['#submit'][] = '_pager_for_node_node_type_form_submit';
  }
}

/**
 * Creating custom form submit handler to save the settings to config file.
 */
function _pager_for_node_node_type_form_submit($form, FormStateInterface $form_state) {
  \Drupal::configFactory()->getEditable('pager_for_node.settings')
    ->set('pager_for_node_' . $form['type']['#default_value'], $form_state->getValue('pager_for_node'))
    ->set('pager_for_node_head_' . $form['type']['#default_value'], $form_state->getValue('pager_for_node_head'))
    ->set('pager_for_node_show_empty_' . $form['type']['#default_value'], $form_state->getValue('pager_for_node_show_empty'))
    ->set('pager_for_node_prev_label_' . $form['type']['#default_value'], $form_state->getValue('pager_for_node_prev_label'))
    ->set('pager_for_node_next_label_' . $form['type']['#default_value'], $form_state->getValue('pager_for_node_next_label'))
    ->set('pager_for_node_first_last_' . $form['type']['#default_value'], $form_state->getValue('pager_for_node_firstlast'))
    ->set('pager_for_node_first_label_' . $form['type']['#default_value'], $form_state->getValue('pager_for_node_first_label'))
    ->set('pager_for_node_last_label_' . $form['type']['#default_value'], $form_state->getValue('pager_for_node_last_label'))
    ->set('pager_for_node_loop_' . $form['type']['#default_value'], $form_state->getValue('pager_for_node_loop'))
    ->set('pager_for_node_random_' . $form['type']['#default_value'], $form_state->getValue('pager_for_node_random'))
    ->set('pager_for_node_random_label_' . $form['type']['#default_value'], $form_state->getValue('pager_for_node_random_label'))
    ->set('pager_for_node_truncate_' . $form['type']['#default_value'], $form_state->getValue('pager_for_node_truncate'))
    ->set('pager_for_node_ellipse_' . $form['type']['#default_value'], $form_state->getValue('pager_for_node_ellipse'))
    ->set('pager_for_node_press_swipe_' . $form['type']['#default_value'], $form_state->getValue('pager_for_node_press_swipe'))
    ->set('pager_for_node_custom_sorting_' . $form['type']['#default_value'], $form_state->getValue('pager_for_node_custom_sorting'))
    ->set('pager_for_node_sort_' . $form['type']['#default_value'], $form_state->getValue('pager_for_node_sort'))
    ->set('pager_for_node_order_' . $form['type']['#default_value'], $form_state->getValue('pager_for_node_order'))
    ->save();
}

/**
 * Implements hook_entity_extra_field_info().
 */
function pager_for_node_entity_extra_field_info() {
  $extra = [];
  foreach (node_type_get_names() as $type => $name) {
    $pager_for_node_node_type = \Drupal::config('pager_for_node.settings')
      ->get('pager_for_node_' . $type);
    if ($pager_for_node_node_type) {
      $extra['node'][$type] = [
        'display' => [
          'pager_for_node_pager' => [
            'label' => t('Pager'),
            'description' => t('Pager for node module content pager.'),
            'weight' => 5,
            'visible' => TRUE,
          ],
        ],
      ];
    }
  }

  return $extra;
}

/**
 * Implements hook_ENTITY_TYPE_view() for node entities.
 */
function pager_for_node_node_view(array &$build, EntityInterface $node, EntityViewDisplayInterface $display, $view_mode) {
  //Include the pager exclusively if it is applicable to the content type of this node.
  if (\Drupal::service('pager_for_node.pager')->pager_for_node_use_pager($node) && $display->getComponent('pager_for_node_pager')) {
    $links = \Drupal::service('pager_for_node.pager')->pager_for_node_build_list($node);
    $build['pager_for_node_pager'] = [
      '#theme' => 'pager_for_node',
      '#list' => $links,
      '#node' => $node,
      '#attached' => [
        'library' => [
          'pager_for_node/drupal.pager_for_node',
        ],
      ],
      '#cache' => [
        'tags'=> ['node_list'],
      ],
    ];

    if (is_object($node)) {
      // Check if we need to support HammerJS.
      if (\Drupal::config('pager_for_node.settings')->get('pager_for_node_press_swipe_' . $node->getType())) {
        $build['pager_for_node_pager']['#attached']['library'][] = 'hammerjs/hammerjs';
        $build['pager_for_node_pager']['#attached']['library'][] = 'pager_for_node/pager_for_node.swipe';
      }
      //Incorporate the previous/next elements into the page header if enabled for this content type.
      if (\Drupal::config('pager_for_node.settings')
        ->get('pager_for_node_head_' . $node->getType())
      ) {
        if (!empty($links['prev']['nid'])) {
          $build['#attached']['html_head_link'][][] = [
            'rel' => 'prev',
            'href' => Url::fromRoute('entity.node.canonical', ['node' => $links['prev']['nid']])
              ->toString(),
          ];
        }
        if (!empty($links['next']['nid'])) {
          $build['#attached']['html_head_link'][][] = [
            'rel' => 'next',
            'href' => Url::fromRoute('entity.node.canonical', ['node' => $links['next']['nid']])
              ->toString(),
          ];
        }
      }
    }
  }
}

/**
 * Implements template_preprocess_hook().
 */
function template_preprocess_pager_for_node(&$vars) {

  if ($node = \Drupal::request()->attributes->get('node')) {
    $vars['node'] = $node;
  }

  $pager_for_node_settings = \Drupal::config('pager_for_node.settings');

  $first_label = t($pager_for_node_settings->get('pager_for_node_first_label_' . $vars['node']->getType()));
  $last_label = t($pager_for_node_settings->get('pager_for_node_last_label_' . $vars['node']->getType()));
  $random_label = t($pager_for_node_settings->get('pager_for_node_random_label_' . $vars['node']->getType()));
  $prev_label = t($pager_for_node_settings->get('pager_for_node_prev_label_' . $vars['node']->getType()));
  $next_label = t($pager_for_node_settings->get('pager_for_node_next_label_' . $vars['node']->getType()));

  $show_empty = $pager_for_node_settings->get('pager_for_node_show_empty_' . $vars['node']->getType());

  if ($pager_for_node_settings->get('pager_for_node_loop_' . $vars['node']->getType())) {
    if (!isset($vars['list']['next']['nid']) && isset($vars['list']['first']['nid'])) {
      $vars['list']['next']['nid'] = $vars['list']['first']['nid'];
    }
    if (!isset($vars['list']['prev']['nid']) && isset($vars['list']['last']['nid'])) {
      $vars['list']['prev']['nid'] = $vars['list']['last']['nid'];
    }
  }

  if ($nav = $vars['list']) {

    if ($pager_for_node_settings->get('pager_for_node_first_last_' . $vars['node']->getType()) != 0) {
      if (isset($nav['first']) && $nav['first']['nid'] != FALSE) {
        $vars['first'] = \Drupal::service('pager_for_node.pager')->pager_for_node_generate_link($nav['first']['nid'], $first_label);
      }
      elseif ($show_empty) {
        $vars['first'] = [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#attributes' => ['class' => ['empty']],
          '#value' => $first_label,
        ];
      }

      if (isset($nav['last']) && $nav['last']['nid'] != FALSE) {
        $vars['last'] = \Drupal::service('pager_for_node.pager')->pager_for_node_generate_link($nav['last']['nid'], $last_label);
      }
      elseif ($show_empty) {
        $vars['last'] = [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#attributes' => ['class' => ['empty']],
          '#value' => $last_label,
        ];
      }
    }

    if ($pager_for_node_settings->get('pager_for_node_random_' . $vars['node']->getType())) {
      $vars['random'] = \Drupal::service('pager_for_node.pager')->pager_for_node_generate_link($nav['random']['nid'], $random_label);
    }

    if (isset($nav['prev']) && $nav['prev']['nid'] != FALSE) {
      $vars['prev'] = \Drupal::service('pager_for_node.pager')->pager_for_node_generate_link($nav['prev']['nid'], $prev_label);
    }
    elseif ($show_empty) {
      $vars['prev'] = [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#attributes' => ['class' => ['empty']],
        '#value' => $prev_label,
      ];
    }

    if (isset($nav['next']) && $nav['next']['nid'] != FALSE) {
      $vars['next'] = \Drupal::service('pager_for_node.pager')->pager_for_node_generate_link($nav['next']['nid'], $next_label);
    }
    elseif ($show_empty) {
      $vars['next'] = [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#attributes' => ['class' => ['empty']],
        '#value' => $next_label,
      ];
    }
  }

  unset($vars['list']);
}
