<?php

namespace Drupal\Tests\pager_for_node\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests that the PagerForNode pagers are appearing.
 *
 * @group pager_for_node
 */
class PagerForNodeFunctionalTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'test_page_test',
    'pager_for_node',
  ];

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';


  /**
   * Disable schema validation.
   *
   * This is super dirty... @todo Should be removed ASAP, when 3.x is released!
   *
   * @var bool
   */
  protected $strictConfigSchema = FALSE;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->config('system.site')->set('page.front', '/test-page')->save();
    $this->user = $this->drupalCreateUser([]);
    $this->adminUser = $this->drupalCreateUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();
    $this->drupalLogin($this->adminUser);

    //Establish a content type for articles intended for testing purposes.
    $type = $this->container->get('entity_type.manager')->getStorage('node_type')
      ->create([
        'type' => 'article',
        'name' => 'Article',
      ]);
    $type->save();
    $this->container->get('router.builder')->rebuild();

    \Drupal::configFactory()
      ->getEditable('pager_for_node.settings')
      ->set('pager_for_node_article', 1)
      ->set('pager_for_node_prev_label_article', 'Previous')
      ->set('pager_for_node_next_label_article', 'Next')
      ->set('pager_for_node_first_label_article', 'First')
      ->set('pager_for_node_last_label_article', 'Last')
      ->set('pager_for_node_random_label_article', 'Random')
      ->save();

    for ($i = 0; $i <= 2; $i++) {
      $node = $this->container->get('entity_type.manager')->getStorage('node')
        ->create([
          'type' => 'article',
          'title' => $i,
        ]);
      $node->save();
    }
  }

  /**
   * Tests if installing the module, won't break the site.
   */
  public function testInstallation() {
    $session = $this->assertSession();
    $this->drupalGet('<front>');
    // Ensure the status code is success:
    $session->statusCodeEquals(200);
    // Ensure the correct test page is loaded as front page:
    $session->pageTextContains('Test page text.');
  }

  /**
   * Tests if uninstalling the module, won't break the site.
   */
  public function testUninstallation() {
    // Go to uninstallation page an uninstall pager_for_node:
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $this->drupalGet('/admin/modules/uninstall');
    $session->statusCodeEquals(200);
    $page->checkField('edit-uninstall-pager_for_node');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    // Confirm uninstall:
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->pageTextContains('The chosen modules have been uninstalled.');
    // Retest the frontpage:
    $this->drupalGet('<front>');
    // Ensure the status code is success:
    $session->statusCodeEquals(200);
    // Ensure the correct test page is loaded as front page:
    $session->pageTextContains('Test page text.');
  }

  /**
   * Make sure pages appear in article node pages.
   */
  public function testPagerOnPage() {
    $session = $this->assertSession();
    // Load the first page.
    $this->drupalGet('/node/1');

    //Verify that the website did not encounter a server error or any other issues.
    $this->assertSession()->statusCodeEquals(200);

    //Ensure that the front page includes the customary text.
    $session->pageTextContains('Next');
    $session->elementTextEquals('css', 'article > div > ul > li > a', 'Next');
    $session->pageTextNotContains('Previous');

    $this->drupalGet('/node/2');

    $this->assertSession()->statusCodeEquals(200);

    $session->pageTextContains('Next');
    $session->elementTextEquals('css', 'article > div > ul.pager_for_node > li.pager_for_node-next > a', 'Next');
    $session->pageTextContains('Previous');
    $session->elementTextEquals('css', 'article > div > ul.pager_for_node > li.pager_for_node-previous > a', 'Previous');

    $this->drupalGet('/node/3');

    //Verify that the website did not generate a server error or any other anomalies.
    $this->assertSession()->statusCodeEquals(200);

    //Validate that the front page displays the prescribed text.
    $session->pageTextNotContains('Next');
    $session->pageTextContains('Previous');
    $session->elementTextEquals('css', 'article > div > ul.pager_for_node > li.pager_for_node-previous > a', 'Previous');
  }

}
